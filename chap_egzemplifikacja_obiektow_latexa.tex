\chapter{\nmu Egzemplifikacja obiektów LaTeXa}
\label{chap:egzemplifikacja-obiektow-latexa}
Niniejszy rozdział został w całości poświęcony zagadnieniom osadzania rozmaitych obiektów w systemie \LaTeX{}, począwszy od tych prostych do nieco bardziej złożonych. Każdy z przykładów będzie poparty odpowiadającym mu kodem źródłowym oraz krótkim komentarzem na co należy zwrócić szczególną uwagę. 

Trudno jednak powtarzać pewne treści kilkukrotnie do każdego z przykładów oraz wtórnie opisywać te same zagadnienia, które nota bene wynikają z poprzedzających je przykładów. Tak więc, budowa przykładów zaawansowanych będzie (ale nie musi) zawierać w sobie pewne podstawowe informacje z elementów podstawowych.

Wysoce istotną sprawą którą należy jeszcze w tym miejscu dopowiedzieć, to fakt istnienia rozległej ilości dostępnych przykładów z obszaru różnych zagadnień, począwszy od list wypunktowanych, poprzez tabele, wzory matematyczne, rysunki, wykresy, schematy, kody źródłowe, a nawet skomplikowane wzory reakcji chemicznych. \\
Liczba pakietów umożliwiająca kreację przeróżnych obiektów jest niewyobrażalnie duża, stąd w niniejszej części zostaną omówione jedynie wybrane zagadnienia\footnote{Zebrane przykłady zostały dobrane w sposób możliwie przystępny dla osób nieposiadających wiedzy na temat systemu \LaTeX{}, natomiast w trakcie postępującej egzemplifikacji stopień trudności przykładów wzrasta.} jedynie w niewielkim stopniu zarysowujące całość dostępnych materiałów.

\LaTeX{} jako środowisko wysoce bogate w szereg dostępnych modułów służących do~ukazania sprecyzowanych struktur, posiada również te fundamentalne, bez których pisząc współczesne dokumenty (w szczególności prace naukowe) trudno się obejść. 

%##############################################################################
\section{\nmu Struktury list}
\label{sec:struktury_list}

Struktury list, to jedne z wielu obiektów jakie można powołać do życia \LaTeX{}u. W~żargonie użytkowników \LaTeX{}a, obiekty takie są to tak zwane ,,środowiska'' (z ang. \textit{environments}). Wśród tych środowisk można wyróżnić kilka układających treści w postaci rozmaitych list tj.: \texttt{itemize}, \texttt{enumerate}, czy \texttt{description}. Każde ze środowisk umożliwia odmienne ułożenie treści, natomiast naturalne nazewnictwo szybko pozwala zorientować się jaki będzie spodziewany wynik końcowy, nawet jeśli nie znamy jeszcze ich działania.

\LaTeX{} wyróżnia 3 główne rodzaje struktur listowania, a są to m.in.:

\begin{itemize}
    \item Listy wypunktowane (przykład \ref{exmp:latex-itemize-list-default})
    \item Listy numeryczne (przykład \ref{exmp:latex-list-enumerate-default})
    \item Niestandardowe listy numeryczne (przykład \ref{exmp:latex-list-enumerate-custom-version-B})
\end{itemize}

%##############################################################################
%\subsection*{Listy wypunktowane}
\begin{exmp}
\label{exmp:latex-itemize-list-default}
Prosta, domyślna lista wypunktowana. Umieszcza domyślny, pierwszy znak dla listy wypunktowanej.

\begin{outputbox}
\begin{itemize}
\item Punkt pierwszy
\item Punkt drugi
\item Punkt trzeci
\item \dots
\item Punkt \texttt{n}-ty
\end{itemize}
\end{outputbox}

\begin{lstlisting}[label=lst:latex-list-itemize-default, caption=Przykład listy wypunktowanej]
\begin{itemize}
\item Punkt pierwszy
\item Punkt drugi
\item Punkt trzeci
\item \dots
\item Punkt \texttt{n}-ty
\end{itemize}
\end{lstlisting}
\end{exmp}

\begin{exmp}
\label{exmp:latex-list-enumerate-default}
Najprostsza lista numeryczna. Jak widać wcięcie w kodzie zupełnie nie wpływa na końcowy wygląd listy. Wcięcie pełni rolę ładniejszej wizualizacji w tekście roboczym.

\begin{outputbox}
\begin{enumerate}
    \item Pierwszy element
    \item Drugi element
    \item Trzeci element
\end{enumerate}
\end{outputbox}

\begin{lstlisting}[label=lst:latex-list-enumerate-default, caption=Prosta lista numeryczna]
\begin{enumerate}
    \item Pierwszy element
    \item Drugi element
    \item Trzeci element
\end{enumerate}
\end{lstlisting}
\end{exmp}


%##############################################################################
\begin{exmp}
\label{exmp:latex-list-enumerate-custom-version-B}
Niestandardowa lista numeryczna.

\begin{outputbox}
\begin{list}{Pytanie \arabic{myOwnCounter}:~}{\usecounter{myOwnCounter}}
\item Element pierwszy
\item Element drugi
\item Element trzeci
\end{list}
\end{outputbox}

W powyższej wizualizacji widać że można dostosowywać nawet dłuższe teksty iteracyjne, które dla większej ich liczby, będą w wygodny sposób automatyzować powtarzalne części tekstu.

\begin{lstlisting}[label=lst:latex-list-custom-wariant-B, caption=Niestandardowa lista]
\begin{list}{Pytanie \arabic{myOwnCounter}:~}{\usecounter{myOwnCounter}}
\item Element pierwszy
\item Element drugi
\item Element trzeci
\end{list}
\end{lstlisting}
\end{exmp}

Aby powyższy przykład zadziałał (przykład \ref{exmp:latex-list-enumerate-custom-version-B}), oprócz umieszczenia kodu z listingu~\ref{lst:latex-list-custom-wariant-B}, należy do preambuły dokumentu dodać linijkę:

\begin{verbatim}
\usecounter{myOwnCounter}
\end{verbatim}

Działanie to pozwoli stworzyć i ustawić nowy, dedykowany licznik, natomiast nazwa \mbox{,,myOwnCounter''}, którą akurat zastosowano w przykładzie może być oczywiście zmieniona na dowolną inną.


%##############################################################################
\section{\nmu Działania matematyczne}
\label{sec:dzialania_matematyczne}
Jedną z głównych ideologii zbudowania systemu \TeX{} była możliwość tworzenia zaawansowanych działań matematycznych w stosunkowo prosty i łatwy sposób, a \LaTeX{} jako późniejsza ewolucja systemu \TeX{}, poprzez utworzenie wygodnych makr dla użytkowników, pozwoliła na tworzenie pięknych, estetycznych i nowoczesnych dokumentów bez względu na system operacyjny.


\subsection*{Tryb matematyczny}
Jak już wiadomo, \LaTeX{} zapewnia wygodny skład równań matematycznych wraz z~pozostałym tekstem. Z tego względu wprowadzono coś co nazywa się trybem matematycznym, oddzielając treści charakterystyczne dla zwykłego tekstu pisanego od równań.

W \LaTeX{}u występują dwie możliwości wprowadzania równań w trybie matematycznym:

\begin{itemize}
\item tryb liniowy (bezpośrednio w tekście)
\item tryb blokowy
\end{itemize}


%##############################################################################
\subsubsection{Tryb liniowy}
Podczas pisania różnych tekstów istnieje czasem potrzeba dołączenia krótkiego fragmentu działania matematycznego bezpośrednio w tekście, nie niszcząc przy tym normalnego składu pisanego dokumentu. Do tego służy liniowy tryb matematyczny, który pozwala umieścić funkcje matematyczne wraz z resztą tekstu.

Tryb ten można wywołać poprzez określone oznaczenia, tj. umieszczenie działania pomiędzy\footnote{Najbardziej popularną i zarazem najkrótszą formą jest zastosowanie otoczenia w postaci pojedynczych znaków dolara (\texttt{\$}), jednakże należy pamiętać, że można spotkać lub stosować inne oznaczenia wywołania trybu matematycznego.}:

\begin{itemize}
\item znakami pojedynczego dolara: \texttt{\red{\$}a + b\red{\$}}

\item lub znakami: \texttt{\red{\textbackslash{}(}a + b\red{\textbackslash{})}}

\item lub pomiędzy elementami: \texttt{\red{\textbackslash{}begin\{math\}}a + b\red{\textbackslash{}end\{math\}}}
\end{itemize}

Wszystkie powyższe rozwiązania (wyróżnione kolorem) są sobie równoważne i mogą być stosowane wymiennie\footnote{Jednakże nie poleca się takiej praktyki. Zamiast tego rekomendowane jest wybranie najwygodniejszego i zarazem intuicyjnego stylu oraz sumienne trzymanie się tej jednej konwencji. Taki zwyczaj pozwoli uniknąć niepotrzebnych omyłek lub błędów podczas kompilacji, spowodowanej w takich sytuacjach zwykle brakiem znaku kończącego komendę.}.

\begin{exmp}
Test pisany połączony właz z trybem matematycznym umieszczonym bezpośrednio w tekście.

\begin{outputbox}
Wybrane działania na pierwiastkach można przedstawić w równoważnych sobie formach np.: $\sqrt[n]{a * b}$ jest tożsame z $\sqrt[n]{a} * \sqrt[n]{b}$, czy $\sqrt[n]{\frac{a}{b}}$ równa się  $\frac{\sqrt[n]{a}}{\sqrt[n]{b}}$.
\end{outputbox}

\begin{lstlisting}[label=lst:latex-math-linear-example-A, caption=Przykład jednego z możliwych zastosowań liniowego trybu matematycznego]
Wybrane działania na pierwiastkach można przedstawić w równoważnych sobie formach np.: $\sqrt[n]{a * b}$ jest tożsame z $\sqrt[n]{a} * \sqrt[n]{b}$, czy $\sqrt[n]{\frac{a}{b}}$ równa się  $\frac{\sqrt[n]{a}}{\sqrt[n]{b}}$.
\end{lstlisting}
\end{exmp}


%##############################################################################
\subsubsection{Tryb blokowy}
Tryb blokowy zwykle służy do wizualnego i estetycznego wyeksponowania rozdzielenia pomiędzy, wzór matematyczny, a tekst pisany. Stosowany jest najczęściej do grupowania algorytmów w niekiedy potężne i długie działania matematyczne, pozwalając jednocześnie zachować ich zgrabne ułożenie w pracy.

Tryb ten podobnie jak tryb liniowy można wywołać poprzez określone oznaczenia, tj.~umieszczenie działania pomiędzy:

\begin{itemize}
\item znakami podwójnego dolara: \texttt{\red{\$\$}a + b\red{\$\$}}

\item lub znakami: \texttt{\red{\textbackslash{}[}a + b\red{\textbackslash{}]}}

\item lub pomiędzy elementami:\\ \texttt{\red{\textbackslash{}begin\{displaymath\}}a + b\red{\textbackslash{}end\{displaymath\}}}
\end{itemize}

Wszystkie powyższe rozwiązania (wyróżnione kolorem) są sobie równoważne i~mogą być stosowane wymiennie. Jednak tak sam jak w trybie liniowym sugeruje utrzymanie jednej konwencji w całej pracy.

%#############################################################################
\pagebreak
\begin{exmp}
Tekst pisany, rozdzielony poprzez wstawienie miedzy niego trybu matematycznego \parencite[s.42]{Cewe08}. Poniżej zostało ukazane przykładowe zastosowanie trybu blokowego na przykładzie definicji.

\begin{outputbox}
Liczbą daną wzorem:

$$
\int_b^a f(x)dx = [F(x)]_a^b = F(b) - F(a)
$$

\noindent , gdzie $F$ jest dowolną funkcją pierwotną funkcji $f$ ciągłej w przedziale $\langle a;b \rangle$, nazywamy \textbf{całką oznaczoną} funkcji $f$ w przedziale $\langle a;b \rangle$. Gdzie $a$ jest dolną granicą całkowania, natomiast $b$ górną granicą całkowania.
\end{outputbox}

\begin{lstlisting}[label=lst:latex-math-block-example-A, caption=Przykład jednego z możliwych zastosowań blokowego trybu matematycznego]
Liczbą daną wzorem:

$$
\int_b^a f(x)dx = [F(x)]_a^b = F(b) - F(a)
$$

\noindent , gdzie $F$ jest dowolną funkcją pierwotną funkcji $f$ ciągłej w przedziale $\langle a;b \rangle$, nazywamy \textbf{całką oznaczoną} funkcji $f$ w przedziale $\langle a;b \rangle$. Gdzie $a$ jest dolną granicą całkowania, natomiast $b$ górną granicą całkowania.
\end{lstlisting}
\end{exmp}

%##############################################################################

\pagebreak
\begin{exmp}
Wykorzystanie środowiska \texttt{array} oraz \texttt{cases} w prostej egzemplifikacji wyznaczania punktów nieciągłości funkcji.

\begin{outputbox}
    Wykorzystanie środowiska \texttt{array} w pierwszym wzorze, dzięki czemu uzyskano ładne wyrównanie tekstu względem kolumn.
    $$
    f(x) = \left\{
    \begin{array}{lcc}
    e^{x+4}               &  \mbox{dla}  &  x \leqslant -4  \\
    \frac{\sin \pi x}{x}  &  \mbox{dla}  &  -4 < x < 0 \\
    \pi \cdot \ln (x+e)   &  \mbox{dla}  &  x \geqslant 0
    \end{array}
    \right.
    $$
    
    W drugim schemacie użyto środowiska \texttt{cases}, które jest nieco przystępniejsze i łatwiejsze w implementacji (ale kosztem wyrównania względem kolumn).
    
    $$
    f(x) =
    \begin{cases}
    \frac{x^4 - 1}{x^2 - 1} \quad \mbox{dla} \quad x \in \mathbb{R} \setminus \lbrace -1,1 \rbrace \\
    0 \quad \mbox{dla} \quad x=-1 \\
    2 \quad \mbox{dla} \quad x=1
    \end{cases}
    $$
\end{outputbox}
    
\begin{lstlisting}[label=lst:latex-math-more-example-F, caption=Wykorzystanie środowiska array oraz cases w egzemplifikacji wyznaczania punktów nieciągłości funkcji]
Wykorzystanie środowiska \texttt{array} w pierwszym wzorze, dzięki czemu uzyskano ładne wyrównanie tekstu względem kolumn.
$$
f(x) = \left\{
\begin{array}{lcc}
e^{x+4}               &  \mbox{dla}  &  x \leqslant -4  \\
\frac{\sin \pi x}{x}  &  \mbox{dla}  &  -4 < x < 0 \\
\pi \cdot \ln (x+e)   &  \mbox{dla}  &  x \geqslant 0
\end{array}
\right.
$$

W drugim schemacie użyto środowiska \texttt{cases}, które jest nieco przystępniejsze i łatwiejsze w implementacji (ale kosztem wyrównania względem kolumn).

$$
f(x) =
\begin{cases}
\frac{x^4 - 1}{x^2 - 1} \quad \mbox{dla} \quad x \in \mathbb{R} \setminus \lbrace -1,1 \rbrace \\
0 \quad \mbox{dla} \quad x=-1 \\
2 \quad \mbox{dla} \quad x=1
\end{cases}
$$
\end{lstlisting}
\end{exmp}


%##############################################################################
\begin{exmp}
Odwołania do poszczególnych wzorów poprzez element \texttt{\textbackslash{}label} z wykorzystaniem do tego granic \parencite[s.340,346,349]{GurgulSuder09}.

\begin{outputbox}
    Prezentowane poniżej wzory m.in. wzór \ref{eq:wzor-granice-1} występujący jako pierwszy,
    \begin{equation}
    g_1 = \log \left( \lim_{n \to \infty} \frac{10^{n+1} + 3 \cdot 2^n}{2 \cdot 10^n - 4} \right)
    \label{eq:wzor-granice-1}
    \end{equation}
    reprezentują pewien dział matematyki nazwany granicami.
    
    \begin{equation}
    g_{10} = \lim_{x \to 0^+} \left( \ln x - 3^{\frac{1}{x}} \right) = \left[ \ln 0^+ - 3^{\frac{1}{0^+}} \right] = \left[ \ln 0^+ - 3^\infty \right]
    \label{eq:wzor-granice-2}
    \end{equation}
    
    \noindent Każdy z pozostałych wzorów (tj.: \ref{eq:wzor-granice-2}, \ref{eq:wzor-granice-3}, \ref{eq:wzor-granice-4}) zawierają pewne unikalne cechy,
    
    \begin{equation}
    g_{100} = \lim_{x \to 0} \frac{\sin 2x}{\sin 5x} = \lim_{x \to 0} \underbrace{\frac{4}{3}}_{\frac{4}{3}} \overbrace{\left( \frac{\sin 4x}{4x} \right)}^{1} = \frac{4}{3} \cdot 1 = \frac{4}{3}
    \label{eq:wzor-granice-3}
    \end{equation}
    
    \noindent stanowiąc przy tym doskonały przykład wzorcowy.
    
    \begin{equation}
    g_{1000} = \left[ \lim_{x \to \infty} \left( 1 + \frac{2}{2x^2 + 3} \right)^{\frac{2x^2 + 3}{2}} \right]^{ \lim_{x \to \infty} \frac{6x^2 + 4x - 2}{2x^2 + 3}}
    \label{eq:wzor-granice-4}
    \end{equation}
\end{outputbox}

\begin{lstlisting}[label=lst:latex-math-more-example-G, caption=Odwołania do wzorów matematycznych]
Prezentowane poniżej wzory m.in. wzór \ref{eq:wzor-granice-1} występujący jako pierwszy,
\begin{equation}
g_1 = \log \left( \lim_{n \to \infty} \frac{10^{n+1} + 3 \cdot 2^n}{2 \cdot 10^n - 4} \right)
\label{eq:wzor-granice-1}
\end{equation}
reprezentują pewien dział matematyki nazwany granicami.

\begin{equation}
g_{10} = \lim_{x \to 0^+} \left( \ln x - 3^{\frac{1}{x}} \right) = \left[ \ln 0^+ - 3^{\frac{1}{0^+}} \right] = \left[ \ln 0^+ - 3^\infty \right]
\label{eq:wzor-granice-2}
\end{equation}

\noindent Każdy z pozostałych wzorów (tj.: \ref{eq:wzor-granice-2}, \ref{eq:wzor-granice-3}, \ref{eq:wzor-granice-4}) zawierają pewne unikalne cechy,

\begin{equation}
g_{100} = \lim_{x \to 0} \frac{\sin 2x}{\sin 5x} = \lim_{x \to 0} \underbrace{\frac{4}{3}}_{\frac{4}{3}} \overbrace{\left( \frac{\sin 4x}{4x} \right)}^{1} = \frac{4}{3} \cdot 1 = \frac{4}{3}
\label{eq:wzor-granice-3}
\end{equation}

\noindent stanowiąc przy tym idealny przykład wzorcowy.

\begin{equation}
g_{1000} = \left[ \lim_{x \to \infty} \left( 1 + \frac{2}{2x^2 + 3} \right)^{\frac{2x^2 + 3}{2}} \right]^{ \lim_{x \to \infty} \frac{6x^2 + 4x - 2}{2x^2 + 3}}
\label{eq:wzor-granice-4}
\end{equation}
\end{lstlisting}
\end{exmp}



%##############################################################################
\pagebreak
\section{\nmu Tabele}
\label{sec:tabele}
Spora liczba zróżnicowanych środowisk \LaTeX{}a umożliwia tworzenie tabel lub ich imitacji poprzez uzyskanie pożądanego przez autora wyglądu końcowego. Przykładów wykorzystania można by podać co najmniej kilkanaście w zależności od porządnych końcowych wariantów, co przekreśla szansę zobrazowania wszystkich możliwych wariacji. Z~tego względu w poniższej części skupiono się jedynie na grupie popularniejszych rozwiązań stosowanych w \LaTeX{}u (tj. \path{table}, \path{tabular}, \path{multirow} oraz \path{hline}), które w dużej liczbie prostych dokumentów, powinny w zupełności wystarczyć.


%##############################################################################
%\subsection{Podstawowe}

\begin{exmp}
\label{exmp:latex-table-basic-example-A}
Wyświetlenie podstawowej tabeli otoczonej pojedynczą linią  z każdej strony. \\
\begin{table}[!h]
    \begin{tabular}{|r|l|}
    \hline
    wiersz1-kolumna1 & wiersz1-kolumna2 \\
    \hline
    wiersz2-kolumna1 & wiersz2-kolumna2 \\
    \hline
    \end{tabular}
\caption{To jest opis tabeli z przykładu \ref{exmp:latex-table-basic-example-A}}
\label{tab:tab:prosta-tabela-przyklad-A}
\end{table}

\begin{lstlisting}[label=lst:latex-table-simple-example-A, caption=Prosta tabela w LaTeX]
\begin{table}[!h]
    \begin{tabular}{|r|l|}
    \hline
    wiersz1-kolumna1 & wiersz1-kolumna2 \\
    \hline
    wiersz2-kolumna1 & wiersz2-kolumna2 \\
    \hline
    \end{tabular}
\caption{To jest opis tabeli z przykładu \ref{exmp:latex-table-basic-example-A}}
\label{tab:tab:prosta-tabela-przyklad-A}
\end{table}
\end{lstlisting}

Jak widać kod opisujący tabele z przykładu \ref{exmp:latex-table-basic-example-A} jest stosunkowo krótki i prosty. Drobnego komentarza wymagają linijki 1, 2, 3, 4, 9 oraz 10, ponieważ opis zawartości tych wierszy w zupełności pozwoli zrozumieć cały przykład oraz ogólną ideologię tworzenia tabel.\\

Wiersz 1 definiuje otoczenie, w którym znajduje się tabela\footnote{W tym przykładzie celowo nie ukazano wyrównywania tabeli w poziomie (np. wyśrodkowując), gdyż zostanie to ukazane w kolejnym przykładzie \ref{lst:latex-table-simple-example-B}.}. Otoczenie to służy przede wszystkim do umieszczania podpisu tabeli (wiersz 9) oraz odnośnika (wiersz 10), dzięki któremu można odwołać się do tabeli. Ważne zapamiętania jest to, że tylko w otoczeniu \texttt{table} możemy definiować wspomniane podpisy  i odnośniki, w innym wypadku spowoduje to prawdopodobnie wyrzuceniem błędu.

W kwadratowych nawiasach (\texttt{[!ht]}) znajdują się parametry przekazywane dla tego otoczenia. W przytoczonym przykładzie to wykrzyknik oznaczający ewentualną negację oraz mała litera litera \texttt{h} od słowa \textit{here} symbolizująca parametr pozycjonujący całą tabele względem tekstu. Podstawowymi parametrami są zwykle małe literki \texttt{b}, \texttt{h} lub \texttt{t}, które oznaczają kolejno \textit{bottom}, \textit{here}, \textit{top} i są równie przyjęte za ogólne oznaczenia w innych otoczeniach.

Wiersz 2 definiuje środowisko \texttt{tabular} w którym umieszczona jest cała zawartość tabeli. Środowisko to posiada 1 parametr, zawierający w sobie liczbę potrzebnych kolumn i sposób justowania tekstu dla każdej z nich. Możliwe parametry wyrównania tekstu to~\texttt{l}, \texttt{r} lub \texttt{c}, oznaczające po kolei \textit{left}, \textit{right}, \textit{center}.
Natomiast pionowa kreska \texttt{|} (z ang. \textit{pipe}) wstawia wertykalne linie pomiędzy kolumny, oddzielając je do siebie.

Wiersz 3 to zwykła pojedyncza pozioma kreska oddzielającą treści poszczególnych wierszy od siebie. Podwójną poziomą kreskę można uzyskać powtarzając komendę jedna po drugiej.

W przykładzie, komenda \texttt{\textbackslash{}hline} umieszczana jest zawsze w nowej linijce tylko ze~względów estetycznych i równie dobrze może być dołączona na końcu poprzedniego wiesza.

Wiersz 4 rozpoczyna już treść zasadniczą tabeli. Znak \texttt{\&}, zwany potocznie ,,etką'' (z~ang. \textit{ampersand}) lub ,,i'', rozdziela treść poszczególnych kolumn od siebie. Zakończenie wiersza zwieńczone jest podwójnym ciach w tył \texttt{\textbackslash\textbackslash}, oznaczający przejście do kolejnego wiersza.
\end{exmp}


%##############################################################################
\begin{exmp}
\label{exmp:latex-table-basic-example-B}
Wyświetlenie nieco bardziej złożonej tabeli w której łączone są poszczególne komórki w pionie oraz poziomie. \\
\begin{table}[!h]
\centering
    \begin{tabular}{c|c|c|c|c}
    \multicolumn{1}{c|}{} & Q & X & Y & Z   \\
    \hline
    \multirow{2}{*}{temp. (\degree{}F)} & \multicolumn{4}{c}{constans} \\
    \hhline{~----}
    & 6.7 & 4.4 & -1.2 & \multirow{2}{*}{9.5} \\
    \hhline{----~}
    \multirow{2}{*}{temp (\degree{}C)} & \multicolumn{2}{c|}{\multirow{2}{*}{constans}} & 12.5 \\
    \hhline{~~~--}          & \multicolumn{2}{c|}{}                      & 0.7 & 1.2 \\
    \hline
    \end{tabular}
\caption{Fikcyjne temperatury na potrzeby przykładu \ref{exmp:latex-table-basic-example-B}}
\label{tab:prosta-tabela-przyklad-B}
\end{table}

\begin{lstlisting}[label=lst:latex-table-simple-example-B, caption=Przykład tabeli w LaTeX łącząca ze sobą wybrane komórki]
\documentclass{article}
\usepackage{multirow}    % http://ctan.org/pkg/multirow
\usepackage{hhline}      % http://ctan.org/pkg/hhline
\usepackage{gensymb}     % dla uzyskania znaczka stopnia

\begin{document}
\begin{table}[!h]
\centering
    \begin{tabular}{c|c|c|c|c}
    \multicolumn{1}{c|}{} & Q & X & Y & Z   \\
    \hline
    \multirow{2}{*}{temp. (\degree{}F)} & \multicolumn{4}{c}{constans} \\
    \hhline{~----}
    & 6.7 & 4.4 & -1.2 & \multirow{2}{*}{9.5} \\
    \hhline{----~}
    \multirow{2}{*}{temp (\degree{}C)} & \multicolumn{2}{c|}{\multirow{2}{*}{constans}} & 12.5 \\
    \hhline{~~~--}  &  \multicolumn{2}{c|}{}  &  0.7 & 1.2 \\
    \hline
    \end{tabular}
\caption{Fikcyjne temperatury na potrzeby przykładu \ref{exmp:latex-table-basic-example-B}}
\label{tab:prosta-tabela-przyklad-B}
\end{table}
\end{document}
\end{lstlisting}

\end{exmp}


%##############################################################################
\pagebreak
\section{\nmu Rysunki}
\label{sec:rysunki}

Jednym z elementarnych ogniw wśród współczesnych systemów składu tekstu jest możliwość osadzania grafiki w pisanym dokumencie. \LaTeX{} jako profesjonale narzędzie w kreacji obszernych tekstów, również udostępnia szereg możliwości załączania fotografii do~dokumentu. 

W poniższych dwóch prostych przykładach można odnaleźć potencjalnie najczęściej stosowane wzorce implementacji rysunków w dokumentach. Przykład~\ref{exmp:latex-single-image-A} sytuuje pojedynczą fotografię w dokumencie, natomiast dla kontrastu przykład \ref{exmp:latex-multi-image-A} warunkuje wykorzystanie więcej, niż jednego rysunku w pojedynczym obiekcie \LaTeX{}a.


\begin{exmp}
    \label{exmp:latex-single-image-A}
    Dołączenie prostej fotografii o zadanej szerokości do dokumentu.
    
    \begin{figure}[!h]
        \centering
        \includegraphics[width=70mm]{images/logo_git_color.png}
        \captionsource{Logotyp systemu kontroli wersji Git}{\url{https://git-scm.com/downloads/logos}}
        \label{fig:logo-git}
    \end{figure}
    
\begin{lstlisting}[label=lst:latex-single-image-A, caption=Pojedyncze zdjęcie o określonej szerokości]
\begin{figure}[!h]
\centering
\includegraphics[width=70mm]{images/logo_git_color.png}
\captionsource{Logotyp systemu kontroli wersji Git}{\url{https://git-scm.com/downloads/logos}}
\label{fig:logo-git}
\end{figure}
\end{lstlisting}
\end{exmp}

\pagebreak
\begin{exmp}
    \label{exmp:latex-multi-image-A}
    Wyświetlenie kilku potencjalnie skorelowanych fotografii do dokumentu.
    
    \begin{figure}[!h]
        \centering
        \begin{subfigure}[b]{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/logo_microsoft_word_2013}
            \caption{Microsoft Word 2013}
            \label{fig:logo-microsoft-word-2013}
        \end{subfigure}%
        \begin{subfigure}[b]{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/logo_libreoffice_writer_version_4}
            \caption{LibreOffice Writer 4}
            \label{fig:logo-libreoffice-writer-4}
        \end{subfigure}%
        \begin{subfigure}[b]{0.3\textwidth}
            \includegraphics[width=\textwidth]{images/logo_texstudio}
            \caption{TeXstudio}
            \label{fig:logo-texstudio}
        \end{subfigure}%
        \caption{Wybrane logotypy edytorów tekstowych przytaczanych w publikacji.}
        \label{fig:sample-logotypes-of-text-editors}
    \end{figure}
    
\begin{lstlisting}[label=lst:latex-single-image-A, caption=Watiant kilku fotografii umieszczonym w jednym obiekcie LaTeXa]
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/logo_microsoft_word_2013}
    \caption{Microsoft Word 2013}
    \label{fig:logo-microsoft-word-2013}
\end{subfigure}%
\begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/logo_libreoffice_writer_version_4}
    \caption{LibreOffice Writer 4}
    \label{fig:logo-libreoffice-writer-4}
\end{subfigure}%
\begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{images/logo_texstudio}
    \caption{TeXstudio}
    \label{fig:logo-texstudio}
\end{subfigure}%
\caption{Wybrane logotypy edytorów tekstowych przytaczanych w publikacji.}
\label{fig:sample-logotypes-of-text-editors}
\end{figure}
\end{lstlisting}
\end{exmp}


%##############################################################################
\section{\nmu Wykresy}
\label{sec:wykresy-zaawansowane}

Obiektami wysoce cenionymi przez ludzką percepcję są bez wątpienia liczne rodzaje wykresów. W prosty do obioru sposób potrafią przekazać duże zestawienia danych w~postaci diagramów kołowych, słupkowych, trójwymiarowych bądź innych. 

Efekt widoczny w poniższych przykładach (tj. \ref{exmp:latex-pie-chart-example-A} oraz \ref{exmp:latex-plot-math-example-A} ) jest niewspółmiernie trudniejszy w realizacji dla edytorów typu WYSIWYG.


\begin{exmp}
\label{exmp:latex-pie-chart-example-A}
Przykładu ukazujący podstawowe zastosowanie biblioteki \path{pgf-pie} do dynamicznego generowania wykresów kołowych typu \textit{pie chart} w oparciu podane wartości liczbowe.

\begin{figure}[!h]
    \centering
    \begin{subfigure}[b]{0.5\textwidth}
        \begin{tikzpicture}%
        \pie[polar, rotate=210]{45/Chrome, 16/IE, 16/Safari, 14/Firefox, 3/Opera, 6/Inne}
        \end{tikzpicture}%
        \caption{Typ wykresu: polar (w zaokrągleniu)}
        \label{fig:pgf-pie-polar}
    \end{subfigure}%
    \begin{subfigure}[b]{0.5\textwidth}%
        \begin{tikzpicture}%
        \pie[square]{44.5/Chrome, 15.6/IE, 16.1/Safari, 14.2/Firefox, 3.3/Opera, 6.3/Inne}
        \end{tikzpicture}%
        \caption{Typ wykresu: square}
        \label{fig:pgf-pie-square}
    \end{subfigure}%
    \captionsource{Wynik zastosowania biblioteki pgf-pie do dynamicznego generowania wykresów}{Dane statystyczne (maj 2015) ze strony \url{http://www.w3counter.com/globalstats.php} na podstawie\parencite{man:pgf-pie}}
\end{figure}


\begin{lstlisting}[label=lst:latex-plot-math-example-A, caption=Przykład zastosowania biblioteli pgf-pie do dynamicznego generowania wykresów]
\begin{figure}[!h]
\centering
\begin{subfigure}[b]{0.5\textwidth}
    \begin{tikzpicture}%
    \pie[polar, rotate=210]{45/Chrome, 16/IE, 16/Safari, 14/Firefox, 3/Opera, 6/Inne}
    \end{tikzpicture}%
    \caption{Typ wykresu: polar (w zaokrągleniu)}
    \label{fig:pgf-pie-polar}
\end{subfigure}%
\begin{subfigure}[b]{0.5\textwidth}%
    \begin{tikzpicture}%
    \pie[square]{44.5/Chrome, 15.6/IE, 16.1/Safari, 14.2/Firefox, 3.3/Opera, 6.3/Inne}
    \end{tikzpicture}%
    \caption{Typ wykresu: square}
    \label{fig:pgf-pie-square}
\end{subfigure}%
\captionsource{Wynik zastosowania biblioteki pgf-pie do dynamicznego generowania wykresów}{Dane statystyczne (maj 2015) ze strony \url{http://www.w3counter.com/globalstats.php} na podstawie\parencite{man:pgf-pie}}
\end{figure}
\end{lstlisting}
    
\end{exmp}

W następnym przykładzie \ref{exmp:latex-plot-math-example-A} przy korzystaniu z pakietu \texttt{TikZ} dla niektórych przykładów należy do argumentów komendy programu kompilującego dokument \LaTeX{}a (w~tym przypadku pdflatex) dodać parametr \path{--shell-escape}, który pozwala na~wywołanie dowolnej komendy powłoki\footnote{Z uwagi na~kwestie bezpieczeństwa systemu należy używać parametru \texttt{-{}-shell-escape} jedynie w~sytuacjach wymagających jego zastosowania.}.



\begin{exmp}
    \label{exmp:latex-plot-math-example-A}
    Przykład ukazujący zastosowanie biblioteki TikZ do dynamicznego generowania wykresów w oparciu od podany wzór matematyczny.
    
    \begin{figure}[!h]
        \centering
        \resizebox{10cm}{!} {
            \begin{tikzpicture}[domain=0:4]
            \draw[very thin,color=gray] (-0.1,-1.1) grid (3.9,3.9);
            \draw[->] (-0.2,0) -- (4.2,0) node[right] {$x$};
            \draw[->] (0,-1.2) -- (0,4.2) node[above] {$f(x)$};
            \draw[color=red] plot[id=x] function{x} 
            node[right] {$f(x) =x$};
            \draw[color=blue] plot[id=sin] function{sin(x)} 
            node[right] {$f(x) = \sin x$};
            \draw[color=orange] plot[id=exp] function{0.05*exp(x)} 
            node[right] {$f(x) = \frac{1}{20} \mathrm e^x$};
            \end{tikzpicture}
        }
        \captionsource{Wynik zastosowania biblioteki TikZ do dynamicznego generowania wykresów}{\url{http://www.texample.net/tikz/examples/gnuplot-basics/}}
    \end{figure}
    
\begin{lstlisting}[label=lst:latex-plot-math-example-A, caption=Przykład zastosowania biblioteli TikZ do dynamicznego generowania wykresów]
\begin{tikzpicture}[domain=0:4]
\draw[very thin,color=gray] (-0.1,-1.1) grid (3.9,3.9);
\draw[->] (-0.2,0) -- (4.2,0) node[right] {$x$};
\draw[->] (0,-1.2) -- (0,4.2) node[above] {$f(x)$};
\draw[color=red] plot[id=x] function{x} 
node[right] {$f(x) =x$};
\draw[color=blue] plot[id=sin] function{sin(x)} 
node[right] {$f(x) = \sin x$};
\draw[color=orange] plot[id=exp] function{0.05*exp(x)} 
node[right] {$f(x) = \frac{1}{20} \mathrm e^x$};
\end{tikzpicture}
\end{lstlisting}
\end{exmp}

Jak można zaobserwować złożona część zestawów informacji może być w bardzo prosty i przystępny sposób zaprezentowana w postaci dynamicznie generowanych wykresów dla~rozmaitych kombinacji danych. Również niewątpliwie dużą zaletą tworzenia diagramów w~systemie \LaTeX{} jest możliwość generowania grafów w oparciu o zewnętrzne technologie tj. bezpośrednia integracja z popularnymi bazami danych MySQL czy Postgres, co tym samym umożliwia tworzenie dokumentów zawierających zawsze najnowszy zestaw danych (bez zbędnej ingerencji w sam dokument pracy).
