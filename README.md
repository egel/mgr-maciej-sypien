# Master Thesis
Master thesis document written by Maciej Sypień

## Basic setup

1. [Install neccesary bibraries like: TeXLive or MikTeX](#installation-of-additional-libs)
2. [Copy repository to your hard drive](#copy-repo)
3. [Compile document](#compile-document)
3. [Become a wizzard](#become-a-wizzard)

### Installation of TeXLive or MikTeX

Firstly you need to install La(Tex) libraries --- If you have *TexLive*, *MikTeX* or similar libraries packages system all ready installed and also biber as well, you can skip this subpoints ;)

##### Linux
For most linux distributions based on [Debian](https://www.debian.org/) like: (ex: [Ubuntu](http://www.ubuntu.com/), [Mint](http://www.linuxmint.com/), [ElementaryOS](http://elementaryos.org/), etc) you could install **texlive** by run this command in terminal:

    sudo apt-get install texlive-full biber

**apt-get** program will install whole bunch of *Tex* libs (~1.5GB). It's a lot of stuff and contains more libs then we'll ever use, but the main advantage of this setp is that you probably won't get any errors like missing common libraries while you'll be writting your thesis --- less errors, less worries - simple as that ;)

##### Windows
You can download [Miktex](http://miktex.org/). It's more-less an equivalent of *Texlive* libs and it's build specially for Windows systems. On Miktex's website you will find all information you need to use this piece of software.

### Copy this repository to your hard drive:

    git clone git@github.com:egel/latex-example.git


##### Install pgf-pie

Install additional library to render plots/charts called [pgf-pie][pgf-pie-ctan]

1. Unzip
2. Create folder `pgf-pie` and copy content into it

  ```
  sudo mv ~/Download/pgf-pie /usr/share/texlive/texmf-dist/tex/latex/
  ```

3. Rebuild latex

  ```
  sudo mktexlsr
  ```

##### Install gnuplot

```
sudo apt-get install gnuplot
```


### Compile whole document

> To work well with some gnu plots we need to add `-shell-escape` to `Options > Commands > pdflatex`
>
> ```
> pdflatex -synctex=1 -interaction=nonstopmode -shell-escape %.tex
> ```

    $ pdflatex document_main.tex
    $ pdflatex document_main.tex
    $ biber bibliografia.tex
    $ pdflatex document_main.tex

### Become a wizzard
All done :)  ...and now you can give a try to become a powerful wizard of LaTeX!



## Maintenance
Quick remove unnecessary files from compilation folder (into terminal)

```
$ rm *.aux *.log *.toc *.lot *.lol *.lot *.out *.dvi *.bbl *.bcf *.blg *.run.xml *.lof *.gz *.gz\(busy\) *.x.table *.sin.table *.exp.gnuplot *.x.gnuplot *.sin.gnuplot *.exp.table *.sin.pgf-plot.gnuplot *.tdo
```

### Compilation to other type of files

LaTeX to plain text (.txt)

    pandoc -s -S main.tex -t plain -o praca.text --biblatex


 [pgf-pie-ctan]: https://www.ctan.org/pkg/pgf-pie
